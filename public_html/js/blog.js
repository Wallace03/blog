/* global Backendless, Handlebars, moment */

$(function () {
    var APPLICATION_ID = "910EA828-77E9-DEE6-FF13-51084C7E5D00",
        SECRET_KEY = "1FA6B682-B70D-CF2B-FF5B-F45749986900",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log("postsCollection");
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment (time).format("dddd, MMMM Do YYYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHtml = blogTemplate(wrapper);
    
    $('.main-container').html(blogHtml);

});

function Posts(args){ 
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.author = args.authorEmail || "";
}
